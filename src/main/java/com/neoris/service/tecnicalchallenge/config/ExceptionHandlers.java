package com.neoris.service.tecnicalchallenge.config;

import com.neoris.service.tecnicalchallenge.model.ExchangeExceptionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlers  {

    @ExceptionHandler(RuntimeException.class)
    Mono<ResponseEntity<ExchangeExceptionResponse>> runtimeExceptionHandler(RuntimeException ex) {
        ExchangeExceptionResponse response = new ExchangeExceptionResponse();
        response.setMessage(ex.getMessage());
        return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response));
    }

}
